
const db = require('./db');
const Query = {
    job: (root,args)=>db.jobs.get(args.id),
    jobs: () => db.jobs.list(),
    companies: () => db.companies.list(),
    company: (root,args)=>db.companies.get(args.id)
}
const Mutation = {
    createJob: (root,{input})=>{
        const jobId =  db.jobs.create(input)
        return db.jobs.get(jobId)
    }
}
const Job = {
    company: (j) => db.companies.get(j.companyId),
}
const Company = {
    jobs: (c) => db.jobs.list().filter(j => j.companyId == c.id),
}
module.exports = { Query,Mutation, Job, Company };