const endpointURL = "http://localhost:9000/graphql"


async function graphQLRequest(query, variables) {
    const response = await fetch(endpointURL, {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify({
            query,
            variables
        })
    })
    const responseBody = await response.json();
    if (responseBody.errors) {
        throw new Error(responseBody.errors.map(error => error.message).join("\r\n"));
    }
    return responseBody.data;
}
export async function loadJobs() {
    const responseBody = await graphQLRequest(`
    {
        jobs {
            id
            title
            company{
                id
                name
            }
        }
    }
    `)
    return responseBody.jobs;
}
export async function loadJob(id) {
    const responseBody = await graphQLRequest(`
    query JobQuery($id:ID!){
        job(id: $id){
          title
          company {
            id
            name
          }
          description
        }
      }
    `, { id })
    return responseBody.job;
}

export async function loadCompany(id) {
    const responseBody = await graphQLRequest(`
    query CompanyQuery($id:ID!){
        company(id: $id){
          name
          description
          jobs {
            id
            title
            company {
                id
                name
            }
          }
        }
      }
    `, { id })
    return responseBody.company;
}

export async function createJob({companyId,title,description}){
    const responseBody = await graphQLRequest(
        `
        mutation CreateJob($input: CreateJobInput){
            job: createJob(input:$input) {
              id
              title
              description
              company {
                id
                name
              }
            }
          }
        `,
        {
            "input":{
              "companyId": companyId,
              "title": title,
              "description": description,
            }
        }
    )
    return responseBody.job
}