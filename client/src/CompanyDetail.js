import React, { Component } from 'react';
import { loadCompany } from './requests';
import { JobList } from './JobList';


export class CompanyDetail extends Component {
  constructor(props) {
    super(props);
    this.state = { company: null };
  }

  componentDidMount() {
    const { companyId } = this.props.match.params;
    loadCompany(companyId).then((company) => this.setState({ company }))
  }
  render() {
    const { company } = this.state;
    return (
      <div>
        <h1 className="title">{company?.name}</h1>
        <div className="box">{company?.description}</div>
        {company ? <JobList jobs={company?.jobs} /> : <></>}
      </div>
    );
  }
}
